require.config ({
    baseUrl: 'app',
    paths: {
        /* lib */
        backbone:           '../../node_modules/backbone/backbone',
        jquery:             '../../node_modules/jquery/dist/jquery',
        underscore:         '../../node_modules/underscore/underscore',
        vue:                '../../node_modules/vue/dist/vue',

        /* m */
        baseModel:          'js/models/baseModel',
        mainModel:          'js/models/mainModel',
        genderModel:        'js/models/genderModel',

        /* v */
        baseView:               'js/views/baseView',
        baseCollectionView:     'js/views/baseCollectionView',
        collectionView:         'js/views/collectionView',
        dialogView:             'js/views/dialogView',
        favoritesView:          'js/views/favoritesView',
        membersView:            'js/views/membersView',
        navView:                'js/views/navView',
        settingsView:           'js/views/settingsView',
        genderView:             'js/views/genderView',
        genderCollectionView:   'js/views/genderCollectionView',
        /*
        mainView:               'js/views/mainView',
        productView:            'js/views/productView',
        */

        /* c */
        baseCollection:     'js/collections/baseCollection',
        collection:         'js/collections/collection',
        genderCollection:   'js/collections/genderCollection',

        /* r */
        baseRoute:          'js/routes/baseRoute',
        route:              'js/routes/route'
    },

    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        }
    }
});

require(['collectionView', 'collection', 'route'], 
    function(collectionView, collection, route) {
        new route;
        Backbone.history.start({pushState: true});

        new collectionView ({
            collection: new collection
        });
    }); 
