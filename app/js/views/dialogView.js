define(['baseView'], 
    function(BaseView) {
        return dialogView = BaseView.extend({
            initialize: function() {
                console.log("settingsView Initialized");
                this.render();
            },

            template: _.template($("#dialog-template").html()),

            render: function() {
                return this.$el.html(this.template(this.model.toJSON()));
            }
        }); 
    });
