define(['baseView', 'dialogView', 'favoritesView', 'membersView', 'navView', 'settingsView'], 
    function(baseView, dialogView, favoritesView, membersView, navView, settingsView) {
        return collectionViewBase = baseView.extend({
            initialize: function() {
                console.log("baseView");
                console.log("collectionViewBase");
                this.fetch();
                this.render();
            }
        });
    });
