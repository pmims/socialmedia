define(['baseView'], 
    function(BaseView) {
        return membersView = BaseView.extend({
            initialize: function() {
                console.log("membersView Initialized");
                this.render();
            },

            template: _.template($("#members-template").html()),

            render: function() {
                return this.$el.html(this.template(this.model.toJSON()));
            }
        }); 
    });
