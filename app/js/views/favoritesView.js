define(['baseView'], 
    function(BaseView) {
        return favoritesView = BaseView.extend({
            initialize: function() {
                console.log("settingsView Initialized");
                this.render();
            },

            template: _.template($("#favorites-template").html()),

            render: function() {
                return this.$el.html(this.template(this.model.toJSON()));
            }
        }); 
    });
