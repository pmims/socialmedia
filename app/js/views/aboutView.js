define(['baseView'], 
    function(BaseView) {
        return aboutView = BaseView.extend({
            initialize: function() {
                this.fetch();
            },

            template: _.template($("#about-template").html())
        }); 
    });
