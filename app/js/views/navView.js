define(['baseView'], 
    function(BaseView) {
        return navView = BaseView.extend({
            initialize: function() {
                console.log("navView init.");
                this.render();
            },

            events: {
                'filter': 'filter'
            },

            filter: function() {
                alert("hello there");
            },

            template: _.template($("#navigation-template").html()),

            render: function() {
                return this.$el.html(this.template(this.model.toJSON()));
            }
        }); 
    });
