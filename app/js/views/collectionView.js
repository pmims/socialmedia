define(['baseCollectionView'], 
    function(BaseCollectionView) {
        return collectionView = BaseCollectionView.extend({
            fetch: function() {
                this.listenTo(this.collection, "reset", this.render);
                this.collection.fetch({reset: true});
            },

            render: function() {
                this.collection.each(function(item) {
                    this.renderModel(item);
                }, this);
            },

            renderModel: function(item) {
                new dialogView({
                    el: "#dialog",
                    model: item
                });

                new favoritesView({
                    el: "#favorites",
                    model: item
                });

                new membersView({
                    el: "#members",
                    model: item
                });

                new navView ({
                    el: "#navigation",
                    model: item
                });

                new settingsView({
                    el: "#settings",
                    model: item
                });
            }
        });
    });
